# S3 Transfer API

## Instructions for use

### Setup

- Setup [AWS CLI](http://aws.amazon.com/cli/) authentication credentials.
- Install S3 Transfer API

```shell
pip install setuptools
python setup.py install
```

- Launch S3 Transfer API, where `<bucket_name>` is the name of the S3 Bucket to transfer from/to and `<tcp_port>` is the TCP port for the API to listen on.

```shell
s3_transfer_api --s3_bucket_name <bucket_name> --tcp_port <tcp_port>
```

### API Usage

Swagger API UI: `http://<hostname>:<tcp_port>/ui`

## Examples

Assuming the S3 Transfer API has been started on `localhost` with a TCP port of `5000`:

### Upload File

```shell
curl.exe -F "upload_file=@test.txt" "http://localhost:5000/upload"
```

### Download File

```shell
curl --request GET "http://localhost:5000/download/test.txt" --output "test.txt"
```
