import setuptools

# load description from README.md
with open("README.md", "r") as fh:
    long_description = fh.read()

# Load requirements from requirements.txt
with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
    name="s3_transfer_api",
    version="0.0.1",
    install_requires=requirements,
    author="Simon Heather",
    author_email="simonheather99@gmail.com",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    package_data={
        '': ['openapi/*.*',
             'static/*.*',
             'templates/*.*'
             ]
    },
    entry_points={
        'console_scripts': [
            's3_transfer_api=s3_transfer_api.__main__:main'
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
