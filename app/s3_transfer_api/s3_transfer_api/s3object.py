"""
This is the s3object module that contains all the REST actions for the S3
Transfer API
"""

import logging
#   https://docs.python.org/3/library/logging.html
import flask
#   https://flask.palletsprojects.com
import connexion
#   https://connexion.readthedocs.io
import boto3
#   https://boto3.amazonaws.com/v1/documentation/api/latest/index.html

from s3_transfer_api import globals

s3 = boto3.resource('s3')


def download(s3_object_key):
    """
    This function responds to a GET request for /download/{s3_object_key}
    with the relevant S3 object

    :param s3_object_key:   s3 object key to read
    :return:                contents of s3 object
    """

    try:
        response = s3.Object(globals.s3_bucket_name, s3_object_key).get()
    except Exception as exception:
        error_code = exception.response['Error']['Code']
        logging.info('Error code:' + error_code)
        if error_code == "NoSuchKey":
            flask.abort(
                404, "S3 object with name {s3_object_key} not found".format(
                    s3_object_key=s3_object_key)
            )
        else:
            flask.abort(
                500,
                "Error downloading {filename}, code: {error_code}".format(
                    filename=file.filename, error_code=error_code)
            )

    return response['Body'].read()


def upload():
    """
    This function responds to a POST request at /upload/{object}

    :return:        200 on success
    """

    file = connexion.request.files['upload_file']

    try:
        s3object = s3.Object(globals.s3_bucket_name, file.filename)
        response = s3object.put(Body=file.read())
    except Exception as exception:
        error_code = exception.response['Error']['Code']
        logging.info('Error code:' + error_code)
        flask.abort(
            500,
            "Error uploading {filename}, code: {error_code}".format(
                filename=file.filename, error_code=error_code)
        )

    return flask.make_response(
        "{filename} successfully uploaded".format(
            filename=file.filename), 200
    )
