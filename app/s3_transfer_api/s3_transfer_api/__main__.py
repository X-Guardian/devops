"""
S3 Transfer API
"""

import argparse
#   https://docs.python.org/3/library/argparse.html
import logging
#   https://docs.python.org/3/library/logging.html
import sys
#   https://docs.python.org/3/library/sys.html
import flask
#   https://flask.palletsprojects.com
import connexion
#   https://connexion.readthedocs.io
import pkg_resources
#   https://setuptools.readthedocs.io/en/latest/pkg_resources.html

from s3_transfer_api import globals
from s3_transfer_api import s3object

openapi_yaml_filename = 'swagger.yml'


def main():
    """
    This function is main
    """

    class argparse_formatter(argparse.ArgumentDefaultsHelpFormatter,
                             argparse.RawDescriptionHelpFormatter):
        pass

    parser = argparse.ArgumentParser(
        formatter_class=argparse_formatter,
        epilog=__doc__
    )
    parser.add_argument('--s3_bucket_name',
                        default=globals.default_s3_bucket_name,
                        help='Specifies the S3 Bucket Name to transfer to/from'
                        )
    parser.add_argument('--tcp_port',
                        default=globals.default_tcp_port,
                        help='Specifies the TCP port to listen on')
    parser.add_argument('--loglevel',
                        choices=['DEBUG', 'INFO',
                                 'WARNING', 'ERROR', 'CRITICAL'],
                        default=globals.default_loglevel,
                        help='Specifies the script logging level')
    args = parser.parse_args()

    log_format = '%(asctime)s: s3_transfer_api: %(levelname)-8s %(message)s'
    logging.basicConfig(stream=sys.stdout,
                        format=log_format,
                        level=args.loglevel)

    go(**vars(args))
    logging.shutdown()


def go(s3_bucket_name=globals.default_s3_bucket_name,
       tcp_port=globals.default_tcp_port,
       loglevel=globals.default_loglevel):
    """
    This function launches the API
    """

    globals.s3_bucket_name = s3_bucket_name

    # Create the application instance
    openapi_dir = pkg_resources.resource_filename(__name__, 'openapi/')
    app = connexion.App(__name__, specification_dir=openapi_dir)
    app.add_api(openapi_yaml_filename)

    # Create a URL route in our application for "/"
    @app.route('/')
    def home():
        """
        This function returns the html home page

        :return:        the rendered template 'home.html'
        """

        return flask.render_template('home.html')

    app.run(host='0.0.0.0', port=tcp_port)


if __name__ == '__main__':
    main()
