module "vpc" {
  name   = "foo-vpc"
  source = "terraform-aws-modules/vpc/aws"

  cidr = var.vpc_cidr

  azs = [
    data.aws_availability_zones.zones.names[0],
    data.aws_availability_zones.zones.names[1],
  ]

  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets

  create_database_subnet_group = false
  enable_dns_support           = true

  private_subnet_tags = local.standard_tags
  public_subnet_tags  = local.standard_tags
}
