output "bastion_host_public_ip" {
    description = "The public IP address of the bastion host"
    value       = aws_instance.bastion.public_ip
}

output "web_server_public_ip" {
    description = "The public IP address of the web server"
    value       = aws_instance.web.public_ip
}
