variable "access_key" {
  description = "Specifies the AWS Access Key"
  type        = string
}

variable "secret_key" {
  description = "Specifies the AWS Secret Key"
  type        = string
}

variable "region" {
  description = "Specifies the AWS region"
  default     = "eu-west-1"
}

variable "vpc_cidr" {
  description = "Specifies the VPC CIDR"
  default     = "10.9.0.0/16"
}

variable "private_subnets" {
  description = "Specifies the VPC private subnets"
  default     = ["10.9.0.0/24", "10.9.1.0/24"]
}

variable "public_subnets" {
  default = ["10.9.10.0/24", "10.9.11.0/24"]
}
variable "database_subnets" {
  default = ["10.9.20.0/24", "10.9.21.0/24"]
}

variable "instance_type" {
  default = "t2.micro"
}

variable "env" {
  description = "Specifies the environment name"
  type        = string
}

variable "author" {
  description = "Specifies the configuration author"
  type        = string
}

variable "key_name" {
  description = "Specifies the key_Name of the of the Key Pair to use for the EC2 instances"
  type        = string
}

variable "bastion_host_name" {
  description = "Specifies the name tag for the bastion host"
  type        = string
}

variable "web_server_name" {
  description = "Specifies the name tag for the web server"
  type        = string
}

variable "s3_bucket_name" {
  description = "Specifies the name for the S3 bucket"
  type        = string
}
