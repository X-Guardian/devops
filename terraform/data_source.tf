data "aws_availability_zones" "zones" {}

data "aws_ami" "amazon_linux" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name = "name"

    values = [
      "amzn2-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

data "aws_ami" "vnova_nginx" {
  owners      = ["self"]
  most_recent = true

  filter {
    name = "name"

    values = [
      "v-nova.nginx",
    ]
  }
}
