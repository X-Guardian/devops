resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = var.instance_type
  key_name               = var.key_name
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.bastion_host.id]

  tags = merge(
    local.standard_tags,
    map("Name", var.bastion_host_name)
  )

  volume_tags = merge(
    local.standard_tags,
    map("Name", var.bastion_host_name)
  )

  lifecycle {
    ignore_changes = [ami]
  }
}

resource "aws_instance" "web" {
  ami                    = data.aws_ami.vnova_nginx.id
  instance_type          = var.instance_type
  key_name               = var.key_name
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.web_server.id]

  tags = merge(
    local.standard_tags,
    map("Name", var.web_server_name)
  )

  volume_tags = merge(
    local.standard_tags,
    map("Name", var.web_server_name)
  )

  lifecycle {
    ignore_changes = [ami]
  }
}
