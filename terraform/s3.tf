resource "aws_s3_bucket" "vnova_s3" {
  bucket = var.s3_bucket_name
  acl    = "private"
  tags   = local.standard_tags
}
