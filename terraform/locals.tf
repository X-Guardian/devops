locals {
  standard_tags = {
    env    = var.env
    author = var.author
  }
}
