# V-Nova Terraform Configuration

This configuration creates AWS resources for the V-Nova recruitment task

## Providers

| Name | Version |
|------|---------|
| aws | 2.46.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| access\_key | Specifies the AWS Access Key | `string` | n/a | yes |
| author | Specifies the configuration author | `string` | n/a | yes |
| bastion\_host\_name | Specifies the name tag for the bastion host | `string` | n/a | yes |
| database\_subnets | n/a | `list` | <pre>[<br>  "10.9.20.0/24",<br>  "10.9.21.0/24"<br>]<br></pre> | no |
| env | Specifies the environment name | `string` | n/a | yes |
| instance\_type | n/a | `string` | `"t2.micro"` | no |
| key\_name | Specifies the key\_Name of the of the Key Pair to use for the EC2 instances | `string` | n/a | yes |
| private\_subnets | Specifies the VPC private subnets | `list` | <pre>[<br>  "10.9.0.0/24",<br>  "10.9.1.0/24"<br>]<br></pre> | no |
| public\_subnets | n/a | `list` | <pre>[<br>  "10.9.10.0/24",<br>  "10.9.11.0/24"<br>]<br></pre> | no |
| region | Specifies the AWS region | `string` | `"eu-west-1"` | no |
| s3\_bucket\_name | Specifies the name for the S3 bucket | `string` | n/a | yes |
| secret\_key | Specifies the AWS Secret Key | `string` | n/a | yes |
| vpc\_cidr | Specifies the VPC CIDR | `string` | `"10.9.0.0/16"` | no |
| web\_server\_name | Specifies the name tag for the web server | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| bastion\_host\_public\_ip | The public IP address of the bastion host |
| web\_server\_public\_ip | The public IP address of the web server |
